
## (C) Ionut Spirlea - ionuts@rdscv.ro

CC = gcc
CFLAGS = -O2  -Wall
LIBS = -lpcap
OBJS = p_speed.o

all: p_speed

p_speed: $(OBJS)
	 $(CC) $(CFLAGS) -o p_speed $(OBJS) $(LIBS)

install: all
	cp p_speed /usr/sbin

clean:
	rm -f *~ *.o p_speed

