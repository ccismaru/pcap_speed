
#ifndef _PCAP_GLOBALS_H
#define _PCAP_GLOBALS_H

#define D_MAX_DEV_LEN		16

#define D_DEFAULT_DEVICE	"eth0"
#define D_DEFAULT_PROMISC	0

#define SNAPLEN			68

typedef struct
{
	float pkts[3];
	float kbps[3];
} T_PCAP_SPEEDS;

typedef struct
{
	unsigned pkts;
	unsigned bytes;
} T_PCAP_COUNTERS;

#endif /* _PCAP_GLOBALS_H */
